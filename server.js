const express = require("express");
var Request = require("request");
var elasticsearch = require("elasticsearch");

const app = express();

var client = new elasticsearch.Client({
  log: [
    {
      level: "error"
    },
    {
      type: "file",
      level: "trace",
      path: "./elasticsearch.log"
    }
  ]
});

let data;
// Fazendo request à API
Request.get(
  "https://jsonplaceholder.typicode.com/users",
  (error, response, body) => {
    if (error) {
      return console.dir(error);
    }
    data = JSON.parse(body);
  }
);

// TRAZER TODOS OS WEBSITES
app.get("/ex1", (req, res) => {
  let websites = [];
  data.map(element => {
    websites.push(element.website);
  });
  res.send(websites);
});

// NOME, EMAIL E EMPRESA QUE TRABALHA
app.get("/ex2", (req, res) => {
  let users = [];
  data.map(element => {
    users.push({
      name: element.name,
      email: element.email,
      company: element.company
    });
  });
  
  // ordenar por nome
  users.sort((a, b) => {
    return a.name > b.name ? 1 : b.name > a.name ? -1 : 0;
  });
  res.send(users);
});

// TODOS QUE CONTENHAM SUITE NO ENDEREÇO
app.get("/ex3", (req, res) => {
  let endereco = [];
  data.map(element => {
    if (element.address.suite.match(/Suite/)) {
      endereco.push(element);
    }
  });
  res.send(endereco);
});

app.listen(3000);
